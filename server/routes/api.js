const express = require('express');
const multer = require('multer');
const path = require('path');
const tinify = require("tinify");

const router = express.Router();

console.log('process.env.TINIFY_KEY', process.env.TINIFY_KEY);

// Configuration
tinify.key = process.env.TINIFY_KEY;

const processImage = ({ file, uploadDirectory }) => {
  return new Promise((resolve, reject) => {
    const destination = path.join(uploadDirectory, file.originalname);

    tinify.fromFile(file.path).toFile(destination, async (error) => {
      if (error) {
        return reject(error);
      }

      return resolve({
        ...file,
        destination
      })
    });
  });
}

const setup = (rootDirectory) => {
  const uploadDirectory = path.join(rootDirectory, 'uploads');
  const upload = multer({ dest: uploadDirectory });

  router.get('/', function(req, res, next) {
    res.json({
      test: 'foo',
      bar: 'THINGS AND TUSS',
      bax: 123123
    });
  });

  router.post('/upload', upload.array('test'), async function(req, res, next) {
    const files = req.files.map(file => {
      file.extension = file.originalname.split('.')[1];
      return file;
    });

    const filePromises = files.map(file => processImage({ file, uploadDirectory }));

    const data = await Promise.all(filePromises);

    console.log('DATA', data);

    res.json(data);
  });

  return router;
}

module.exports = setup;
